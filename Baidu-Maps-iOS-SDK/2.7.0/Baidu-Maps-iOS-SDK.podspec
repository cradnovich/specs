Pod::Spec.new do |s|

  s.name         = "Baidu-Maps-iOS-SDK"
  s.version      = "2.7.0"
  s.summary      = "Baidu Maps SDK for iOS"

  s.description  = <<-DESC
		   Baidu Maps iOS SDK is a set of APIs, base on iOS 4.3 or later. 
		   Offer some basic API for map presentation, also provides POI search
		   , path planning, map anotation, offline maps, location and powerful
		   LBS feature.  
                   DESC

  s.homepage     = "http://developer.baidu.com/map/sdk-ios.htm"

  s.license      = { 
	:type => 'Copyright', 
	:text => 'LICENSE  ©2015 Baidu, Inc. All rights reserved' 
  }

  s.authors            = { "Tangdixi" => "Tangdixi@gmail.com" }

  s.platforms     = {:ios => nil}

  s.source       = { 
	:http => "http://developer.baidu.com/map/static/doc/output_ios_a/map_search_cloud_loc_util/BaiduMap_IOSSDK_v2.7.0_Lib.zip"
  }

  s.prepare_command = "\t\t\tlipo -create Release-iphoneos/libbaidumapapi.a Release-iphonesimulator/libbaidumapapi.a -output libBaiduMapApi.a\n"
  
  s.source_files = "inc/*.h"
  
  s.resources = "*.bundle"
  
  s.preserve_paths = "*.a"
  
  s.frameworks = 'CoreLocation','QuartzCore','OpenGLES','SystemConfiguration','CoreGraphics','Security'
  
  s.libraries = 'BaiduMapApi','stdc++'
  
  s.requires_arc = true

  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/Baidu-Maps-iOS-SDK' }

end

