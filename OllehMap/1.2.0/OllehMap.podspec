Pod::Spec.new do |s|

  s.name         = "OllehMap"
  s.version      = "1.2.0"
  s.summary      = "Korean Olleh Map 올레맵 지도 iOS SDK"

  s.description  = <<-DESC
                  스마트한 지도 라이프~
                  올레맵 API를 소개합니다!

                  * KT 자체 구축한 고품질 공간정보
                  * 다양한 기능의 기본 API
                  * 폭 넓은 서비스 개발 환경 지원
                  * 고객 맞춤형 API 서비스 제공
                   DESC

  s.homepage     = "https://www.ollehmap.com/ios-sdk/"

  s.license      = { 
	:type => 'Copyright', 
	:text => 'Copyright © KT Hitel. All Right Reserverd.' 
  }

  s.author            = { "KT Hitel" => "jh.ban@kt.com" }
  
  s.platform     = :ios, '7.0'
  
  # This zip has binary library files for modern architectures, isn't linked from the download page, and headers/classes do not match the documentation
  s.source       = {
    :http => "https://www.ollehmap.com/download/ios/GIOP-iOS-SDK_V.1.2.zip"
  }

  s.prepare_command = "\t\t\tlipo -create lib/libGIOPMap.a lib/libGIOPMapSim.a -output libOllehMap.a\n"
  
  s.source_files = "header/*.h"
  
  s.resource_bundles = { "OllehMap" => ["resource/*.png"] }
  
  s.preserve_paths = "*.a"
  
  s.frameworks = 'CoreLocation','QuartzCore','UIKit','SystemConfiguration','CoreGraphics','Security', 'CFNetwork','MobileCoreServices','Foundation'
  
  s.libraries = 'stdc++', 'z'
  
  s.vendored_libraries = 'libOllehMap.a'
    
  s.requires_arc = true

  # https://jira.mongodb.org/browse/SERVER-10644?focusedCommentId=436684&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-436684
  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/OllehMap', 'OTHER_LDFLAGS' => '--stdlib=libstdc++' }

end

